package com.tlz.rerectiveandroid

import io.reactivex.Observable
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.disposables.Disposable
import io.reactivex.rxkotlin.plusAssign
import io.reactivex.subjects.BehaviorSubject

interface PropertyType<T> {

    val value: T?

    val observable: Observable<T>

    fun subscribe(onNext: (T) -> Unit) = observable.subscribe(onNext)

    fun subscribe(onNext: (T) -> Unit, onError: (Throwable) -> Unit) = observable.subscribe(onNext, onError)

    fun subscribe(onNext: (T) -> Unit, onError: (Throwable) -> Unit, onComplete: () -> Unit) = observable.subscribe(onNext, onError, onComplete)

}

interface MutablePropertyType<T> : PropertyType<T> {

    override var value: T?

}

class Property<T>(init: T) : PropertyType<T> {

    override val observable: Observable<T>
        get() = sink

    private var _value: T = init
        set(value) {
            field = synchronized(this) {
                sink.onNext(value)
                value
            }
        }

    override val value: T = _value
        get() = synchronized(this) {
//            field = _value
            field
        }

    private val sink = BehaviorSubject.createDefault<T>(init)

    private val disposables = CompositeDisposable()

    constructor(propertyType: PropertyType<T>) : this(propertyType.value!!) {
        disposables += (propertyType.observable.subscribe({
            _value = it
        }, {
            throw UnsupportedOperationException("Property doesn't support onError")
        }, {
            disposables.dispose()
        }))
    }

}

class MutableProperty<T>(init: T?) : MutablePropertyType<T> {

    constructor() : this(null)

    override val observable: Observable<T>
        get() = sink.filter { it != null }

    override var value: T? = init
        get() = synchronized(this) { field }
        set(value) {
            field = synchronized(this) {
                sink.onNext(value!!)
                value
            }
        }

    private val sink = if(init == null) BehaviorSubject.create() else BehaviorSubject.createDefault<T>(init)

    fun bindTo(observable: Observable<T>) = modify(observable)

    fun bindTo(propertyType: PropertyType<T>) = modify(propertyType.observable)

    private fun modify(observable: Observable<T>): Disposable {
        return observable.subscribe({
            value = it
        }, {
            throw UnsupportedOperationException("Property doesn't support onError")
        }, {
            //do nothing
        })
    }

}

